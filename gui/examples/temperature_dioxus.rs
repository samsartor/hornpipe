use dioxus::prelude::*;
use hornpipe_core::tracing_on;
use hornpipe_gui::dioxus::{use_hornpipe, BiInput};
use hornpipe_interface::{hornpipe_extern, RefTx, SlotTx, Text};

fn main() {
    tracing_on();
    dioxus_desktop::launch(app);
}

hornpipe_extern! {
    pub struct Temperature {
        computed deg_c: f64,
        computed deg_f: f64,
    }
}

impl Temperature {
    fn deg_c(this: RefTx<Self>) -> f64 {
        (this.deg_f().opt().get().unwrap_or(41.0) - 32.0) * 5.0 / 9.0
    }

    fn deg_f(this: RefTx<Self>) -> f64 {
        this.deg_c().opt().get().unwrap_or(5.0) * 9.0 / 5.0 + 32.0
    }
}

fn app(cx: Scope) -> Element {
    use_hornpipe(&cx, |tx| {
        let state = use_state(&cx, Temperature::new).attach(&tx);
        let parser = |src: &str, dst: SlotTx<f64>| match src.parse::<f64>() {
            Ok(v) => Ok(dst.set(v)),
            Err(_) => Err(Text::new(&format!("{src:?} is not a float"))),
        };
        let shower = |v: SlotTx<f64>| Text::new(&format!("{:.2}", v.get()));
        cx.render(rsx! {
            div {
                BiInput {
                    id: "deg_c",
                    value: state.deg_c().detached,
                    parser: parser,
                    shower: shower,
                }
                label {
                    r#for: "deg_c",
                    "Celsius",
                }
            }
            div {
                BiInput {
                    id: "deg_f",
                    value: state.deg_f().detached,
                    parser: parser,
                    shower: shower,
                }
                label {
                    r#for: "deg_f",
                    "Fahrenheit",
                }
            }
        })
    })
}
