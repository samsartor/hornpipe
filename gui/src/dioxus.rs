use dioxus::core::{Scope, ScopeId};
use dioxus::prelude::*;
use hornpipe_interface::{hornpipe_extern, Null, Own, Ref, Slot, SlotTx, Text, Transaction};
use std::any::type_name;
use std::panic::{catch_unwind, resume_unwind, AssertUnwindSafe};
use std::{sync::Arc, task::Wake};
use tracing::trace_span;

#[derive(Clone)]
struct HornpipeWaker {
    scheduler: Arc<dyn Fn(ScopeId) + Sync + Send>,
    scope: ScopeId,
}

impl Wake for HornpipeWaker {
    fn wake(self: std::sync::Arc<Self>) {
        (self.scheduler)(self.scope);
    }
}

#[tracing::instrument(skip(cx, func), level = "trace")]
pub fn use_hornpipe<O, F>(cx: &ScopeState, func: F) -> O
where
    F: FnOnce(&Transaction) -> O,
{
    use hornpipe_core::{create_now, logic, Element, ObjectOwn, SlotElement};

    struct HornpipeHookInner {
        waker: ObjectOwn,
    }

    let waker = use_state(cx, || {
        let waker = Arc::new(HornpipeWaker {
            scheduler: cx.schedule_update_any(),
            scope: cx.scope_id(),
        });
        let waker = create_now(
            &[],
            [Element::Null.slot(), SlotElement::Waker(waker.into())],
        );
        waker.store_label(format_args!("dioxus waker for {}", type_name::<F>()));
        HornpipeHookInner { waker }
    })
    .waker
    .slot_id(0);

    let tx = Transaction::start();
    tx.refer()
        .store_label(format_args!("dioxus rerender for {}", type_name::<F>()));
    logic::start_computation(tx.refer(), tx.guard(), waker);
    let out = catch_unwind(AssertUnwindSafe(|| func(&tx)));
    logic::finish_computation(tx.refer(), tx.guard(), &mut []);
    match out {
        Ok(out) => {
            tx.commit();
            out
        }
        Err(panic) => resume_unwind(panic),
    }
}

#[derive(Props)]
pub struct BiInputProps<'a, T> {
    id: &'a str,
    value: Slot<T>,
    parser: fn(&str, SlotTx<T>) -> Result<(), Own<Text>>,
    shower: fn(SlotTx<T>) -> Own<Text>,
    refer: Option<Slot<Option<Ref<BiInputState<T>>>>>,
}

hornpipe_extern! {
    pub struct BiInputState<T> {
        mut source: Slot<T>,
        computed(dyn) draft: Own<Text>,
        mut error: Option<Own<Text>>,
    }
}

#[allow(non_snake_case)]
pub fn BiInput<'a, T: Send + Sync + 'static>(cx: Scope<'a, BiInputProps<'a, T>>) -> Element {
    let BiInputProps {
        id,
        value,
        parser,
        shower,
        refer,
    } = *cx.props;
    use_hornpipe(&cx, move |tx| {
        let state = use_state(&cx, || {
            BiInputState::new(
                value,
                move |this| shower(this.source().get().expect("BiInput is detached")),
                Null,
            )
        });
        state.refer().label("biinput state");
        let state = state.attach(tx);
        if let Some(refer) = refer.and_then(|refer| refer.attach(&tx)) {
            refer.set(Some(state.detached));
        }
        let (style, title) = match state.error().get() {
            Some(err) => ("border-color: red;", err.as_str()),
            None => ("", ""),
        };

        let draft = state.draft().get();

        cx.render(rsx! {
            input {
                id: "{id}",
                r#type: "text",
                value: "{draft}",
                style: "{style}",
                title: "{title}",
                oninput: move |evt| {
                    let span = trace_span!("biinput_oninput", evt=?evt);
                    let _span = span.enter();
                    let tx = Transaction::start();
                    let state = state.detached.attach(&tx).unwrap();
                    if let Some(slot) = value.attach(&tx) {
                        match parser(&evt.data.value, slot) {
                            Ok(_) => state.error().set(Null),
                            Err(err) => state.error().set(Some(err)),
                        }
                    }
                    state.draft().set(Text::new(&evt.data.value));
                    tx.commit();
                },
            }
        })
    })
}
