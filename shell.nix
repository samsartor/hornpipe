{ pkgs ? import <nixpkgs> {} }:
with pkgs; mkShell (let
  buildInputs = [
    # Misc stuff for *-sys crates    
    rustup
    binutils
    cmake
    clang
    libclang.lib
    lld
    pkg-config

    # Stuff for dioxus
    glib
    gtk3
    webkitgtk_4_1
    libsoup_3

    # Stuff for rendering
    libGL
    vulkan-tools
    vulkan-loader

    # WINIT_UNIX_BACKEND=wayland
    libxkbcommon
    wayland

    # WINIT_UNIX_BACKEND=x11
    xorg.libXcursor
    xorg.libXrandr
    xorg.libXi
    xorg.libX11
  ];
  in {
  name = "hornpipe-gui"; 
  buildInputs = buildInputs;
  shellHook =
  ''
    export LD_LIBRARY_PATH="${lib.makeLibraryPath buildInputs}"
    export XDG_DATA_DIRS=${builtins.getEnv "XDG_DATA_DIRS"};
  '';
})
