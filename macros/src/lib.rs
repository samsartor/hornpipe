extern crate proc_macro;

use proc_macro::TokenStream;
use proc_macro2::TokenStream as TokenStream2;
use quote::{format_ident, quote, quote_spanned, ToTokens};
use syn::{
    parse_macro_input, parse_quote, spanned::Spanned as _, DataEnum, DataStruct, DataUnion, DeriveInput, Error, Fields, Generics, Ident, Visibility
};
use synstructure::{BindingInfo, Structure};

#[proc_macro_attribute]
pub fn state(_attr: TokenStream, item: TokenStream) -> TokenStream {
    todo!()
}

fn write_partially_owned_projections(
    vis: &Visibility,
    rootident: &Ident,
    generics: &Generics,
    fields: &Fields,
) -> Result<TokenStream2, Error> {
    let fieldsname = format_ident!("PartiallyOwned{rootident}");
    let struc = fields
        .into_iter()
        .enumerate()
        .map(|(i, syn::Field { ident, ty, vis, attrs, .. })| {
            let field = match ident {
                Some(ident) => ident.to_token_stream(),
                None => format_ident!("_{i}").to_token_stream(),
            };
            for attr in attrs {
                match attr.meta.require_path_only() {
                    Ok(path) if path.is_ident("unsafe_skip") => return Ok(quote!{ #vis #field: &'a mut #ty, }),
                    _ => (),
                }
            }
            Ok(quote! { #vis #field: &'a mut hornpipe_core::share::PartiallyOwned<'b, #ty>, })
        })
        .map(|t| t.unwrap_or_else(Error::into_compile_error))
        .collect::<TokenStream2>();
    let strucproj = fields
        .into_iter()
        .enumerate()
        .map(|(i, syn::Field { ident, attrs, ty, .. })| {
            let i = syn::Index::from(i).to_token_stream();
            let infield = match ident {
                Some(ident) => ident.to_token_stream(),
                None => i.clone(),
            };
            let outfield = match ident {
                Some(ident) => ident.to_token_stream(),
                None => format_ident!("_{i}").to_token_stream(),
            };
            for attr in attrs {
                match attr.meta.require_path_only() {
                    Ok(path) if path.is_ident("unsafe_skip") => return Ok(quote! { #outfield: &mut (*self.as_ptr_mut()).#infield, }),
                    _ => (),
                }
            }
            Ok(quote! {
                #outfield: hornpipe_core::share::PartiallyOwned::new_mut(&mut (*(self.as_ptr_mut() as *mut Self)).#infield),
            })
        })
        .map(|t| t.unwrap_or_else(Error::into_compile_error))
        .collect::<TokenStream2>();
    let funcs = fields
        .into_iter()
        .enumerate()
        .map(|(i, syn::Field { ident, ty, vis, attrs, .. })| {
            for attr in attrs {
                match attr.meta.require_path_only() {
                    Ok(path) if path.is_ident("unsafe_skip") => return Ok(quote!{}),
                    _ => (),
                }
            }
            let i = syn::Index::from(i).to_token_stream();
            let field = match ident {
                Some(ident) => ident.to_token_stream(),
                None => i.clone(),
            };
            let doc = format!("Projection for [{field}](#structfield.{field}).");
            let (func_ref, func_mut, func_ptr) = match ident {  
                Some(ident) => (format_ident!("{ident}_ref"), format_ident!("{ident}_mut"), format_ident!("{ident}_ptr")),
                None => (format_ident!("ref{i}"), format_ident!("mut{i}"), format_ident!("ptr{i}")),
            };
            Ok(quote! {
                #[doc = #doc]
                #vis fn #func_ref<'a, 'b>(self: &'a hornpipe_core::share::PartiallyOwned<'b, Self>) -> &'a hornpipe_core::share::PartiallyOwned<'b, #ty> {
                    unsafe { self.offset(hornpipe_core::macro_exports::offset_of!(Self, #ident)) }
                }
                
                #[doc = #doc]
                #vis fn #func_mut<'a, 'b>(self: &'a mut hornpipe_core::share::PartiallyOwned<'b, Self>) -> &'a mut hornpipe_core::share::PartiallyOwned<'b, #ty> {
                    unsafe { self.offset_mut(hornpipe_core::macro_exports::offset_of!(Self, #ident)) }
                }

                #[doc = #doc]
                #vis fn #func_ptr<'a>(self: *mut hornpipe_core::share::PartiallyOwned<'a, Self>) -> *mut hornpipe_core::share::PartiallyOwned<'a, #ty> {
                    unsafe { (&mut (*(self as *mut Self)).#field) as *mut #ty as *mut _ }
                }
            })
        })
        .map(|t| t.unwrap_or_else(Error::into_compile_error))
        .collect::<TokenStream2>();
    let (impl_generics, type_generics, where_clause) = generics.split_for_impl();
    
    let mut fields_generics: Generics = parse_quote!(<'a, 'b>);
    fields_generics.params.extend(generics.params.iter().cloned());
    fields_generics.where_clause = generics.where_clause.clone();
    let (fields_impl_generics, fields_type_generics, fields_where_clause) = fields_generics.split_for_impl();


    Ok(quote! {
        #vis struct #fieldsname #fields_impl_generics #fields_where_clause {
            #struc
        }

        impl #impl_generics #rootident #type_generics #where_clause {
            #funcs

            #vis fn project_mut<'a, 'b>(self: &'a mut hornpipe_core::share::PartiallyOwned<'b, Self>) -> #fieldsname #fields_type_generics {
                unsafe { #fieldsname {
                    #strucproj
                } }
            }
        }
    })
}

fn write_tx_projections(
    rootident: &Ident,
    generics: &Generics,
    fields: &Fields,
) -> Result<TokenStream2, Error> {
    let funcs = fields
        .into_iter()
        .enumerate()
        .map(|(i, syn::Field { ident, ty, vis, .. })| {
            let i = syn::Index::from(i).to_token_stream();
            let field = match ident {
                Some(ident) => ident.to_token_stream(),
                None => i.clone(),
            };
            let doc = format!("Projection for [{field}](#structfield.{field}).");
            let func = match ident {
                Some(ident) => ident.clone(),
                None => format_ident!("get{i}"),
            };
            Ok(quote! {
                #[doc = #doc]
                #vis fn #func<'tx>(self: hornpipe_core::TxRef<'tx, Self>) -> hornpipe_core::TxRef<'tx, #ty> {
                    unsafe { hornpipe_core::TxRef::offset(
                        self,
                        hornpipe_core::macro_exports::offset_of!(Self, #field) as u32,
                    ) }
                }
            })
        })
        .map(|t| t.unwrap_or_else(Error::into_compile_error))
        .collect::<TokenStream2>();
    let (impl_generics, type_generics, where_clause) = generics.split_for_impl();
    Ok(quote! {
        impl #impl_generics #rootident #type_generics #where_clause {
            #funcs
        }
    })
}

fn write_data_impl(
    ident: &Ident,
    generics: &Generics,
    fields: &Fields,
    universe: &syn::Path,
) -> Result<TokenStream2, Error> {
    let count = fields.len();
    let pairs = fields
        .into_iter()
        .enumerate()
        .map(|(i, syn::Field { ident, ty, .. })| {
            let i = syn::Index::from(i).to_token_stream();
            let field = match ident {
                Some(ident) => quote!( #ident ),
                None => quote!( #i ),
            };
            Ok(quote_spanned! {
                ty.span() => (
                    hornpipe_core::macro_exports::offset_of!(Self, #field) as isize,
                    hornpipe_core::data::raw_operations::<#ty>(),
                ),
            })
        })
        .map(|t| t.unwrap_or_else(Error::into_compile_error))
        .collect::<TokenStream2>();
    let finalize_calls = fields
        .into_iter()
        .enumerate()
        .map(|(i, syn::Field { ident, ty, .. })| {
            let i = syn::Index::from(i).to_token_stream();
            let field = match ident {
                Some(ident) => quote!( #ident ),
                None => quote!( #i ),
            };
            quote_spanned! {
                ty.span() => <#ty as hornpipe_core::data::Data>::finalize(
                    hornpipe_core::share::PartiallyOwned::new_mut(&mut (*(internal.as_ptr_mut() as *mut Self)).#field),
                    hornpipe_core::share::PartiallyOwned::new_mut(&mut (*(outgoing.as_ptr_mut() as *mut Self)).#field),
                    ctx.fork(),
                );
            }
        })
        .collect::<TokenStream2>();
    let (impl_generics, type_generics, where_clause) = generics.split_for_impl();
    Ok(quote! {
        impl #impl_generics hornpipe_core::Data  for #ident #type_generics #where_clause {
            type Universe = #universe;
            
            fn finalize<'temp, 'global: 'temp>(
                internal: &'temp mut hornpipe_core::share::PartiallyOwned<'temp, Self>,
                outgoing: &'temp mut hornpipe_core::share::PartiallyOwned<'global, Self>,
                mut ctx: hornpipe_core::transaction::LifecycleContext<Self::Universe>,
            ) {
                unsafe { #finalize_calls }
            }
        }
    })
}

fn write_copyref_impl(mut s: synstructure::Structure) -> TokenStream2 {
    s.bind_with(|_| synstructure::BindStyle::Move);
    s.filter(|b| {
        for attr in &b.ast().attrs {
            let Ok(name) = attr.meta.require_path_only() else { continue };
            if name.is_ident("unsafe_skip") {
                return false;
            }
        }
        true
    });

    let visit_owned = s.each(|b| quote! { hornpipe_core::share::CopyRef::visit_owned(#b, callback) });
    let set_partial_ownership = s.each(|b| quote! { hornpipe_core::share::CopyRef::set_partial_ownership(hornpipe_core::share::PartiallyOwned::new_mut(#b), matching, status) });
    let partial_drop = s.each(|b| quote! { hornpipe_core::share::CopyRef::partial_drop(hornpipe_core::share::PartiallyOwned::new_mut(#b)) });

    s.gen_impl(quote! {
        gen unsafe impl hornpipe_core::share::CopyRef for @Self {
            fn visit_owned(this: &Self, callback: &mut (impl FnMut(hornpipe_core::framing::DataPtr) + ?Sized)) {
                unsafe { match this { #visit_owned } }
            }
            
            unsafe fn set_partial_ownership(this: &mut hornpipe_core::share::PartiallyOwned<Self>, matching: u8, status: hornpipe_core::share::OwnershipStatus) {
                unsafe { match this.as_mut() { #set_partial_ownership } }
            }

            unsafe fn partial_drop(this: &mut hornpipe_core::share::PartiallyOwned<Self>) {
                unsafe { match this.as_mut() { #partial_drop } }
            }
        }
    })
}

#[proc_macro_derive(Data, attributes(universe))]
pub fn derive_data(input: TokenStream) -> TokenStream {
    let d = parse_macro_input!(input as DeriveInput);
    let copy_ref_impl = write_copyref_impl(Structure::new(&d));

    let DeriveInput { ident, generics, data , attrs, .. } = d;

    let tokens = (|| {
        let mut universe = parse_quote!(hornpipe_core::DefaultUniverse);
        for attr in attrs {
            let syn::MetaNameValue { path, value, .. } = attr.meta.require_name_value()?;
            if path.is_ident("universe") {
                let syn::Expr::Lit(syn::ExprLit {
                    lit: syn::Lit::Str(lit),
                    ..
                }) = value
                else {
                    return Err(Error::new(
                        value.span(),
                        format_args!("should be a type string like \"DefaultUniverse\""),
                    ));
                };
                universe = lit.parse()?;
            }
        }
        let fields = match data {
            syn::Data::Struct(DataStruct { fields, .. }) => fields,
            syn::Data::Enum(DataEnum { enum_token, .. }) => {
                return Err(Error::new(
                    enum_token.span,
                    format_args!("{ident} must be a struct"),
                ))
            }
            syn::Data::Union(DataUnion { union_token, .. }) => {
                return Err(Error::new(
                    union_token.span,
                    format_args!("{ident} must be a struct"),
                ))
            }
        };
        Ok([
            Ok(copy_ref_impl),
            write_data_impl(&ident, &generics, &fields, &universe),
            write_tx_projections(&ident, &generics, &fields),
        ]
        .into_iter()
        .map(|t| t.unwrap_or_else(Error::into_compile_error))
        .collect())
    })();
    TokenStream::from(tokens.unwrap_or_else(Error::into_compile_error))
}

#[proc_macro_derive(ProjectPartiallyOwned, attributes(unsafe_skip))]
pub fn derive_project_partially_owned(input: TokenStream) -> TokenStream {
    let DeriveInput {
        ident,
        generics,
        data,
        vis,
        ..
    } = parse_macro_input!(input as DeriveInput);
    let tokens = (|| {
        let fields = match data {
            syn::Data::Struct(DataStruct { fields, .. }) => fields,
            syn::Data::Enum(DataEnum { enum_token, .. }) => {
                return Err(Error::new(
                    enum_token.span,
                    format_args!("{ident} must be a struct"),
                ))
            }
            syn::Data::Union(DataUnion { union_token, .. }) => {
                return Err(Error::new(
                    union_token.span,
                    format_args!("{ident} must be a struct"),
                ))
            }
        };

        write_partially_owned_projections(&vis, &ident, &generics, &fields)
    })();
    TokenStream::from(tokens.unwrap_or_else(Error::into_compile_error))
}

#[proc_macro_derive(CopyRef, attributes(unsafe_skip))]
pub fn derive_copyref(input: TokenStream) -> TokenStream {
    let d = parse_macro_input!(input as DeriveInput);
    let tokens = write_copyref_impl(Structure::new(&d));
    TokenStream::from(tokens)
}
