How do transactions work? Since a transaction keeps a separate copy of all accessed data, we make separate copies of all accessed data. And then to commit data, we commit to the copy being the actual data. I know, you can say it, I'm a fricking geneous (how is that spelled?).

Oh and because making real copies is expensive, we just call `memcpy` and get all the same bits. Totally fool proof! Wait, that might be the reason why why everything is so unsafe... huh

Ok the actual idea here is:
1. when datatype needs mutation, the parent object gets `memcpy`ed into the transaction so the mutations can be done to the copy
2. if the transaction aborts, the copy is discarded
3. if the transaction commits, the copy's pointer is swapped into [[the indirection]]
4. the original swapped out pointer gets shared among existing transactions, in order to maintain isolation

And here are the problems we need to solve:
- Not only are memory locations unstable, but the same object may exist at _multiple locations simultaneously_. This basically rules out any interior mutability, and makes futex in particular completely unsound. The `Opaque<T>` wrapper eliminates this problem by putting `T` behind a `Box`.
- Ownership becomes pretty complicated. Say a transaction T overwrites A with B inside an object that also contains C. If T aborts then T owns B and the indirection owns A, C. Whereas if T commits then T owns A and the indirection owns B, C. This requires some runtime metadata to keep straight.
- Copies need to get rebased on any intermediary changes before committing. This makes the ownership problem even worse.