// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use crate::framing::DataPtr;
use crate::{self as hornpipe_core, declare_universe};
use core::fmt;
pub use hornpipe_macros::{CopyRef, ProjectPartiallyOwned};
use std::marker::PhantomData;
use std::mem::{transmute, ManuallyDrop};
use std::ops::{Deref, DerefMut};

/// Shared access is safe for shallow bitwise copies of this type. Exclusive access may not be safe.
/// This type may have drop logic.
///
/// `T: CopyRef` for all `T: Copy` but not the other way around.
///
/// Note that implementing [CopyRef] is almost always unsound for any type with checked
/// interior mutability, since multiple shallow copies of the locking primitive will not
/// lock one another. Instead those types must live behind a pointer such as [crate::Opaque].
///
/// The implementor must also guarantee that method calls are passed down to any fields which
/// can be accessed to through a mutable borrow of the `PartiallyOwned` type.
pub unsafe trait CopyRef {
    /// Call the given function on any directly owned data pointers.
    ///
    /// This should recurse through any type _except_ `Own`. So the caller must recurse themselves to
    /// visit any pointers owned by the pointers provided to callback.
    ///
    /// Strictly speaking it is legal for implementors to not visit all owned data pointers (see
    /// [Opaque]), but that will cause strange behavior (if not actual UB). Notably, some references
    /// may appear valid long after the parent objects have actually been killed.
    fn visit_owned(this: &Self, callback: &mut (impl FnMut(DataPtr) + ?Sized));

    unsafe fn set_partial_ownership(
        this: &mut PartiallyOwned<Self>,
        matching: u8,
        status: OwnershipStatus,
    );

    unsafe fn partial_drop(this: &mut PartiallyOwned<Self>);
}

// SAFETY: `T` is `Copy`, this is the only easy case!
unsafe impl<T: Copy> CopyRef for T {
    unsafe fn set_partial_ownership(
        _this: &mut PartiallyOwned<Self>,
        _matching: u8,
        _status: OwnershipStatus,
    ) {
    }
    unsafe fn partial_drop(_this: &mut PartiallyOwned<Self>) {}
    fn visit_owned(_this: &Self, _callback: &mut (impl FnMut(DataPtr) + ?Sized)) {}
}

/// A wrapper around any [CopyRef] type which might be a bitwise copy of some original
/// instance in 'owner. Parts of self can be fully owned by this wrapper instead, although
/// this must be tracked at runtime by [OwnershipFlagged] (or a similar type). If this wrapper
/// is dropped, only partially owned sections have drop logic run.
///
/// This wrapper prevents exclusive access to `!Copy` types, like an extra-unfriendly `ManuallyDrop`.
#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct PartiallyOwned<'owner, T: ?Sized + CopyRef> {
    owner: PhantomData<&'owner T>,
    value: ManuallyDrop<T>,
}

impl<'owner, T: CopyRef> PartiallyOwned<'owner, T> {
    pub unsafe fn wrap(value: T) -> Self {
        PartiallyOwned {
            owner: PhantomData,
            value: ManuallyDrop::new(value),
        }
    }

    #[inline(always)]
    pub unsafe fn take(&mut self) -> T {
        ManuallyDrop::take(&mut self.value)
    }

    #[inline(always)]
    pub unsafe fn as_mut(&mut self) -> &mut T {
        &mut *self.value
    }

    #[inline(always)]
    pub unsafe fn offset<S: CopyRef>(&self, offset: usize) -> &PartiallyOwned<'owner, S> {
        unsafe { transmute(transmute::<_, *const u8>(self).offset(offset as isize)) }
    }

    #[inline(always)]
    pub unsafe fn offset_mut<S: CopyRef>(
        &mut self,
        offset: usize,
    ) -> &mut PartiallyOwned<'owner, S> {
        unsafe { transmute(transmute::<_, *mut u8>(self).offset(offset as isize)) }
    }

    #[inline(always)]
    pub unsafe fn offset_ptr<S: CopyRef>(
        self: *mut Self,
        offset: usize,
    ) -> *mut PartiallyOwned<'owner, S> {
        unsafe { transmute(transmute::<_, *mut u8>(self).offset(offset as isize)) }
    }

    #[inline(always)]
    pub fn as_ptr(&self) -> *const T {
        &*self.value
    }

    #[inline(always)]
    pub fn as_ptr_mut(&mut self) -> *mut T {
        &mut *self.value
    }
}

impl<'owner, T: CopyRef> PartiallyOwned<'owner, T> {
    #[inline(always)]
    pub fn copy(value: &'owner T) -> Self {
        // SAFETY: this defines the safety requirement on [CopyRef] impls
        let copy = unsafe { std::ptr::read(value) };
        let mut new = PartiallyOwned {
            owner: PhantomData,
            value: ManuallyDrop::new(copy),
        };
        unsafe {
            T::set_partial_ownership(&mut new, !0, OwnershipStatus::Copied);
        }
        new
    }

    #[inline(always)]
    pub fn copy_pending(value: &'owner T) -> Self {
        // SAFETY: this defines the safety requirement on [CopyRef] impls
        let copy = unsafe { std::ptr::read(value) };
        let mut new = PartiallyOwned {
            owner: PhantomData,
            value: ManuallyDrop::new(copy),
        };
        unsafe {
            T::set_partial_ownership(&mut new, !0, OwnershipStatus::Pending);
        }
        new
    }

    #[inline(always)]
    pub fn new_ref<'b: 'owner>(value: &'b T) -> &Self {
        // Self is transparent and exactly as permissive as a bare reference
        unsafe { &*(value as *const T as *const Self) }
    }

    /// The caller must ensure that internal ownership flags are correctly set,
    /// and that `value` is not later fully dropped with some copied data inside.
    #[inline(always)]
    pub unsafe fn new_mut(value: &mut T) -> &mut Self {
        // Self is transparent, and the caller takes on remaining responsibility
        unsafe { &mut *Self::new_ptr(value) }
    }

    #[inline(always)]
    pub unsafe fn new_ptr(value: *mut T) -> *mut Self {
        value as *mut Self
    }

    pub unsafe fn upgrade_pending_to_owned(&mut self) {
        T::set_partial_ownership(self, OwnershipStatus::Pending as u8, OwnershipStatus::Owned);
    }

    pub unsafe fn downgrade_owned_to_pending(&mut self) {
        T::set_partial_ownership(self, OwnershipStatus::Pending as u8, OwnershipStatus::Owned);
    }

    /// Replace `self` with a copy and move out the current value. The returned value can not
    /// provide exclusive access or run drop logic even if `self` was the previous owner, because
    /// the new copy in `self` may still live. If the caller can guarantee that all copies are dead,
    /// they can call [Self::upgrade_pending_to_owned] to restore full ownership to the returned value.
    pub fn move_out(&mut self) -> Self {
        let copy = PartiallyOwned::copy(&**self);
        // SAFETY: the copy will remain valid because the owner is set to `pending` status,
        // which prevents exclusive access
        unsafe {
            let copy = transmute(copy);
            let mut this = std::mem::replace(self, copy);
            this.downgrade_owned_to_pending();
            this
        }
    }
}

impl<'owner, T: ?Sized + CopyRef> Deref for PartiallyOwned<'owner, T> {
    type Target = T;

    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        // SAFETY: further copies might be made via this reference, but they will
        // freeze self so exclusive access through [OwnershipFlagged::get_mut] is
        // not possible
        &*self.value
    }
}

impl<'owner, T: ?Sized + Copy> DerefMut for PartiallyOwned<'owner, T> {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut *self.value
    }
}

impl<'owrer, T: ?Sized + CopyRef> Drop for PartiallyOwned<'owrer, T> {
    fn drop(&mut self) {
        // SAFETY: we are wrapped in a ManuallyDrop, so there will be no further access of self
        unsafe {
            T::partial_drop(self);
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum OwnershipStatus {
    Uninit = 0,
    Owned = 1,
    Pending = 2,
    Copied = 4,
}

impl OwnershipStatus {
    pub fn matches(self, bitflags: u8) -> bool {
        (self as u8 & bitflags) != 0
    }
}

#[derive(Clone, Debug, ProjectPartiallyOwned)]
pub struct OwnershipFlagged<T: CopyRef> {
    // TODO: represent this with an enum, rather than T+bool
    raw: T,
    status: OwnershipStatus,
}

impl<T: Default + CopyRef> Default for OwnershipFlagged<T> {
    fn default() -> Self {
        Self::new_owned(T::default())
    }
}

unsafe impl<T: CopyRef> CopyRef for OwnershipFlagged<T> {
    unsafe fn set_partial_ownership(
        this: &mut PartiallyOwned<Self>,
        matching: u8,
        status: OwnershipStatus,
    ) {
        let this_status = this.status_mut();
        if this_status.matches(matching) {
            **this_status = status;
        }
    }

    unsafe fn partial_drop(this: &mut PartiallyOwned<Self>) {
        if this.status == OwnershipStatus::Pending {
            #[cfg(debug_assertions)]
            panic!("dropped value pending ownership")
        } else if this.status == OwnershipStatus::Owned {
            unsafe {
                drop(this.raw_mut().take());
            }
        }
    }

    fn visit_owned(this: &Self, callback: &mut (impl FnMut(DataPtr) + ?Sized)) {
        if this.status == OwnershipStatus::Owned || this.status == OwnershipStatus::Pending {
            T::visit_owned(&this.raw, callback)
        }
    }
}

#[derive(Debug)]
pub struct NotOwned;

impl fmt::Display for NotOwned {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "not owned")
    }
}

impl std::error::Error for NotOwned {}

impl<T: CopyRef> OwnershipFlagged<T> {
    // TODO: not sure about the safety of this yet. It might be fine?
    pub const fn new_owned(raw: T) -> Self {
        OwnershipFlagged {
            raw,
            status: OwnershipStatus::Owned,
        }
    }

    pub fn into_inner(self) -> T {
        self.raw
    }

    pub const fn new<'owner>(raw: T) -> PartiallyOwned<'owner, Self>
    where
        T: 'owner,
    {
        PartiallyOwned {
            owner: PhantomData,
            value: ManuallyDrop::new(OwnershipFlagged {
                raw,
                status: OwnershipStatus::Owned,
            }),
        }
    }

    pub fn is_owner(&self) -> bool {
        self.status as u8 & (OwnershipStatus::Owned as u8 | OwnershipStatus::Pending as u8) != 0
    }

    pub fn get_ref<'a>(self: &'a PartiallyOwned<Self>) -> &'a T {
        self.raw_ref()
    }

    pub fn get_mut<'a>(self: &'a mut PartiallyOwned<Self>) -> Result<&'a mut T, NotOwned> {
        if **self.status_ref() == OwnershipStatus::Owned {
            // SAFETY: the flag is true and we have &mut self, which is only possible
            // if ownership was gained after creating self, since creating self requires
            // that [OwnershipFlagged]::after_copy is called
            Ok(unsafe { self.raw_mut().as_mut() })
        } else {
            Err(NotOwned)
        }
    }

    pub fn get_mut_or_clone<'a>(
        self: &'a mut PartiallyOwned<Self>,
        clone: impl FnOnce(&T) -> T,
    ) -> &'a mut T {
        if **self.status_ref() == OwnershipStatus::Owned {
            // SAFETY: the flag is true and we have &mut self, which is only possible
            // if ownership was gained and no further copies made.
            return unsafe { self.raw_mut().as_mut() };
        }

        let x = clone(self.get_ref());
        // no drop can be run, becasue we do not have exclusive access anyway
        *self = OwnershipFlagged::new(x);
        match self.get_mut() {
            Ok(x) => x,
            Err(NotOwned) => unreachable!(),
        }
    }

    pub fn swap_owned(
        self: &mut PartiallyOwned<Self>,
        other: &mut PartiallyOwned<Self>,
    ) -> Result<(), NotOwned>
    where
        T: CopyRef + 'static,
    {
        use OwnershipStatus::*;

        match (**self.status_ref(), **other.status_ref()) {
            (Owned | Pending, Owned | Pending) => {
                unsafe {
                    std::mem::swap(
                        transmute::<_, &mut PartiallyOwned<'static, Self>>(self),
                        transmute::<_, &mut PartiallyOwned<'static, Self>>(other),
                    );
                }
                Ok(())
            }
            _ => Err(NotOwned),
        }
    }

    pub fn replace_with_owned(self: &mut PartiallyOwned<Self>, value: T) -> Option<T>
    where
        T: CopyRef,
    {
        let owned = if **self.status_ref() == OwnershipStatus::Owned {
            // SAFETY: the flag is true , which is only possible
            // if ownership was gained after creating self, since creating self requires
            // that [OwnershipFlagged]::after_copy is called
            Some(unsafe { self.raw_mut().take() })
        } else {
            None
        };
        // ownership just got moved out, above
        **self.status_mut() = OwnershipStatus::Uninit;
        // no drop should be run because `owned` will drop later
        *self = OwnershipFlagged::new(value);
        owned
    }

    pub fn replace_with_copy<'owner>(
        self: &mut PartiallyOwned<'owner, Self>,
        value: &'owner T,
    ) -> Option<T>
    where
        T: CopyRef,
    {
        let owned = if **self.status_ref() == OwnershipStatus::Owned {
            // SAFETY: the flag is true, which is only possible
            // if no copies have been made since creating ownership
            Some(unsafe { self.raw_mut().take() })
        } else {
            None
        };
        **self.status_mut() = OwnershipStatus::Copied;
        // no drop should be run because `owned` will drop later
        *self.raw_mut() = PartiallyOwned::copy(value);
        owned
    }
}

impl<T: CopyRef> Deref for OwnershipFlagged<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.raw
    }
}

impl<T: CopyRef> DerefMut for OwnershipFlagged<T> {
    // SAFETY: it is OK to have exclusive access when not wrapped in PartiallyOwned
    fn deref_mut(&mut self) -> &mut T {
        &mut self.raw
    }
}

#[test]
fn test_partially_owned_data_dropped_only_once() {
    declare_universe!(Test);

    use crate::data::Opaque;
    use std::rc::{Rc, Weak};

    let r = Rc::new(());
    let w = Rc::downgrade(&r);
    let count = || -> usize { Weak::strong_count(&w) };

    let original = OwnershipFlagged::new(Opaque::new_in(r, Test));
    assert_eq!(count(), 1);
    let mut copy_1 = PartiallyOwned::copy(&*original);
    assert_eq!(count(), 1);
    let mut copy_2 = PartiallyOwned::copy(&*original);
    assert_eq!(count(), 1);

    copy_2.replace_with_owned(Opaque::new_in(original.clone(), Test));
    assert_eq!(count(), 2);
    copy_2.replace_with_copy(&original);
    assert_eq!(count(), 1);
    drop(copy_2);

    assert_eq!(count(), 1);
    copy_1.replace_with_copy(&original);
    assert_eq!(count(), 1);
    drop(copy_1);
    assert_eq!(count(), 1);

    drop(original);
    assert_eq!(count(), 0);
}

/// ```compile_fail
/// use crate::data::Opaque;

/// let x = OwnershipFlagged::new(Opaque::new(vec![42]));
/// let y = PartiallyOwned::copy(&*x);
/// let x_shared = y.get_ref();
/// let x_exclusive = x.get_mut().unwrap();
/// let _ = (x_shared, x_exclusive);
/// ```
mod _compile_fail_tests {}
