// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//! Responsible for stopping the universe for an instant during submit.

use crate::{DefaultUniverse, Universe};
use parking_lot::RwLock;
use std::cell::UnsafeCell;
use std::marker::PhantomData;
use std::ops::Deref;
use std::{any, fmt};

pub struct Unihandle<U: Universe = DefaultUniverse> {
    _unsafe_to_construct: (),
    _universe: PhantomData<U>,
}

pub type UnihandleLock<U> = RwLock<Unihandle<U>>;

pub trait UnihandleGuard<U: Universe>: Deref<Target = Unihandle<U>> {
    fn unlocked(&mut self, func: impl FnOnce());
}

impl<'a, U: Universe> UnihandleGuard<U> for parking_lot::RwLockReadGuard<'a, Unihandle<U>> {
    fn unlocked(&mut self, func: impl FnOnce()) {
        parking_lot::RwLockReadGuard::unlocked(self, func)
    }
}

impl<'a, U: Universe> UnihandleGuard<U>
    for parking_lot::RwLockUpgradableReadGuard<'a, Unihandle<U>>
{
    fn unlocked(&mut self, func: impl FnOnce()) {
        parking_lot::RwLockUpgradableReadGuard::unlocked(self, func)
    }
}

/// Construct a new handle, which is used to lock instances of [Unitex<U>].
///
/// SAFETY: only one instance of `Unihandle<U>` can ever exist for a given `U`. This is
/// generally the sole responsibility of `declare_universe!`.
pub const unsafe fn new_universe_handle<U: Universe>() -> Unihandle<U> {
    Unihandle {
        _unsafe_to_construct: (),
        _universe: PhantomData,
    }
}

/// An rwlock which shares its locked state with every other Unitex in the universe.
#[repr(transparent)]
pub struct Unitex<T, U: Universe = DefaultUniverse> {
    inner: UnsafeCell<T>,
    _universe: PhantomData<U>,
}

unsafe impl<U: Universe, T: Send> Sync for Unitex<T, U> {}
unsafe impl<U: Universe, T: Send> Send for Unitex<T, U> {}

impl<U: Universe, T> fmt::Debug for Unitex<T, U> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Unitex(_: {})", any::type_name::<T>())
    }
}

impl<U: Universe, T: Default> Default for Unitex<T, U> {
    #[inline]
    fn default() -> Self {
        Self::new(T::default())
    }
}

impl<U: Universe, T> Unitex<T, U> {
    #[inline]
    pub const fn new(inner: T) -> Self {
        Self {
            inner: UnsafeCell::new(inner),
            _universe: PhantomData,
        }
    }

    #[inline]
    pub fn read<'a>(&'a self, _read: &'a Unihandle<U>) -> &'a T {
        // SAFETY:
        // - inner is valid and fully initilized
        // - inner is immutable _while the handle is heald_
        unsafe { &*self.inner.get() }
    }

    #[inline]
    pub fn write<'a>(&'a self, _write: &'a mut Unihandle<U>) -> &'a mut T {
        // SAFETY:
        // - inner is valid and fully initilized
        // - exclusive access to inner is maintained through the handle
        unsafe { &mut *self.inner.get() }
    }

    pub fn map_write<'a, S>(
        &'a self,
        write: &mut Unihandle<U>,
        func: impl FnOnce(&mut T) -> &mut S,
    ) -> &'a Unitex<S, U> {
        let out = func(self.write(write));
        let ptr = out as *mut S;
        // SAFETY:
        // - S and Unitex<S> have the same layout
        // - because func was able to produce excluse access of S from &mut T,
        //   we know the returned borrow will live as long as self
        // - because func was able to produce excluse access of S from &mut T,
        //   we know that the returned unitex will be able to produce exclusive access if locked
        // TODO: will these arguments convince MIRI?
        unsafe { &*(ptr as *mut Unitex<S, U>) }
    }

    #[inline]
    pub fn from_mut<'a>(ptr: &'a mut T) -> &'a Self {
        let ptr = ptr as *mut T;
        // SAFETY:
        // - T and Self have the same layout
        // - the returned borrow will never outlive ptr
        // - the returned mutex starts can have exclusive access if locked
        unsafe { &*(ptr as *mut Self) }
    }

    #[inline]
    pub fn as_mut(&mut self) -> &mut T {
        self.inner.get_mut()
    }

    pub unsafe fn as_ptr(&self) -> *mut T {
        self.inner.get()
    }
}
