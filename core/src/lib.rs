// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#![feature(
    offset_of_enum,
    offset_of_nested,
    arbitrary_self_types,
    const_black_box
)]
#![cfg_attr(doc, feature(fundamental))]

pub mod data;
pub mod framing;
mod refs;
mod register;
pub mod share;
#[cfg(test)]
mod test;
pub mod transaction;
pub mod tree;
pub mod universe;
pub mod utex;

pub use crossbeam_epoch::{pin, Guard};
pub use data::{Data, Operations};
pub use refs::{Own, Ref, TxRef};
pub use register::{Counter, Register};
pub use transaction::Transaction;
pub use universe::{DefaultUniverse, Universe};

pub use hornpipe_macros::{state, Data};

#[doc(hidden)]
pub mod macro_exports {
    pub use once_cell;
    pub use std::mem::offset_of;
}

#[cfg(all(feature = "tracing", feature = "tracing-subscriber"))]
/// Enable very detailed debug logs for debugging purposes.
pub fn tracing_on() {
    use tracing::metadata::LevelFilter;
    tracing::subscriber::set_global_default(
        tracing_subscriber::FmtSubscriber::builder()
            .with_max_level(LevelFilter::TRACE)
            .finish(),
    )
    .unwrap();
}

#[cfg(not(all(feature = "tracing", feature = "tracing-subscriber")))]
/// Enable very detailed debug logs for debugging purposes.
pub fn tracing_on() {}
